'use strict';

const mongoose = require('mongoose');
const Product = mongoose.model('Product');

exports.get = async () => {
    const res = await Product.find({
        active: active
    });
    return res;
};

exports.getBySlug = async (slug) => {
    const res = await Product.find({
        slug: slug,
        active: true
    }, 'title slug price');
    return res;
};

exports.getOneBySlug = async (slug) => {
    const res = await Product.findOne({
        slug: slug
    });
    return res;
};

exports.getById = async (id) => {
    const res = await Product.findById(id);
    return res;
};

exports.getByTag = async (tag) => {
    const res = await Product.find({
        tags: tag,
        active: true
    }, 'title description price slug tags');
    return res;
};


exports.create = async (data) => {
    return await new Product(data).save();
}

exports.update = async (data, id) => {
    const res = await Product.findByIdAndUpdate(id, {
        $set: {
            tag: data.tag,
            title: data.title,
            active: data.active
        }
    });
    return res;
}

exports.delete = async (data, id) => {
    await Product.findOneAndRemove(id);
}