'use strict';

const mongoose = require('mongoose');
const Order = mongoose.model('Order');

exports.get = async () => {
    const res = await Order.find({}, 'number status customer items')
        .populate('customer', 'name')
        .populate('items.product', 'title');
    return res;
};

exports.getByNumber = async (orderNumber) => {
    const res = await Order.find({
        orderNumber: orderNumber,
    });
    return res;
};

exports.getById = async (id) => {
    const res = await Order.findById(id);
    return res;
};

exports.create = async (data) => {
    return await new Order(data).save();
}

exports.update = async (data, id) => {
    const res = await Order.findByIdAndUpdate(id, {
        $set: {
            customer: customer,
            price: price,
            status: status,
            items: items
        }
    });
    return res;
}

exports.delete = async (data, id) => {
    await Order.findOneAndRemove(id);
}