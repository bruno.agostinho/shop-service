'use strict';

const mongoose = require('mongoose');
const Customer = mongoose.model('Customer');

exports.get = async () => {
    const res = await Customer.find({
        active: active
    });
    return res;
};


exports.authenticate = async (data) => {
    return await Customer.findOne({
        email: data.email,
        password: data.password
    });
};

exports.getByName = async (name) => {
    const res = await Customer.find({
        name: name,
    });
    return res;
};

exports.getById = async (id) => {
    const res = await Customer.findById(id);
    return res;
};

exports.create = async (data) => {
    return await new Customer(data).save();
}

exports.update = async (data, id) => {
    const res = await Customer.findByIdAndUpdate(id, {
        $set: {
            name: data.name,
            email: data.email,
            password: data.password,
            status: status
        }
    });
    return res;
}

exports.delete = async (data, id) => {
    await Customer.findOneAndRemove(id);
}