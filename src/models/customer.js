'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    name: {
        type: String,
        required: [true, 'O campo name é obrigatório']
    },
    email: {
        type: String,
        required: [true, 'O campo email é obrigatório']
    },
    password: {
        type: String,
        required: [true, 'O campo password é obrigatório'],
    },
    status: {
        type: Boolean,
        required: [true, 'O campo status é obrigatório'],
    }
});

module.exports = mongoose.model('Customer', schema);