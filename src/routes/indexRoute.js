'use strict'

const express = require('express');
const indexRouter = express.Router();

indexRouter.get('/', (req, res, next) => {
    res.status(200).send({
        title: "GET",
        version: "0.0.1"
    });
});


indexRouter.post('/', (req, res, next) => {
    res.status(200).send({
        title: "POST",
        version: "0.0.1"
    });
});

module.exports = indexRouter;