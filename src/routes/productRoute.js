 'use strict'

 const express = require('express');
 const productRouter = express.Router();
 const productController = require('../controllers/productController');

 productRouter.post('/', productController.post);
 productRouter.put('/:id', productController.put);
 productRouter.delete('/delete/:id', productController.delete);
 productRouter.get('/all', productController.get);
 productRouter.get('/:id', productController.getById);
 productRouter.get('/slug/:slug', productController.getBySlug);
 productRouter.get('/oneSlug/:slug', productController.getOneBySlug);
 productRouter.get('/tags/:tag', productController.getByTag);

 module.exports = productRouter;