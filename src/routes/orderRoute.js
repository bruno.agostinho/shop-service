 'use strict'

 const express = require('express');
 const orderRouter = express.Router();
 const orderController = require('../controllers/orderController');

 orderRouter.post('/', orderController.post);
 orderRouter.put('/:id', orderController.put);
 orderRouter.delete('/delete/:id', orderController.delete);
 orderRouter.get('/all', orderController.get);
 orderRouter.get('/:id', orderController.getById);
 orderRouter.get('/order/:number', orderController.getByNumber);

 module.exports = orderRouter;