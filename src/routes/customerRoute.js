 'use strict'

 const express = require('express');
 const customerRouter = express.Router();
 const customerController = require('../controllers/customerController');
 const authSerice = require('../services/authService');

 customerRouter.post('/', authSerice.authorize, customerController.post);
 customerRouter.put('/:id', authSerice.authorize, customerController.put);
 customerRouter.delete('/delete/:id', authSerice.authorize, customerController.delete);
 customerRouter.get('/all', authSerice.authorize, customerController.get);
 customerRouter.get('/:id', authSerice.authorize, customerController.getById);
 customerRouter.get('/customer/:name', authSerice.authorize, customerController.getByName);

 module.exports = customerRouter;