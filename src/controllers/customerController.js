'use strict'

const ValidatorContract = require('../validator/validator');
const CustomerDAO = require('../dao/customerDAO');
const md5 = require('md5');
const emailService = require('../services/emaiService');
const authService = require('../services/authService');


exports.get = async (req, res, next) => {
    try {
        var searchedData = await CustomerDAO.get();
        res.status(200).send(searchedData);
    } catch (e) {
        res.status(500).send({
            messageErro: e,
            message: 'Erro ao realizar operação'
        });
    }
}

exports.getByName = async (req, res, next) => {
    try {
        var searchedData = await CustomerDAO.getByName(req.params.name)
        res.status(200).send(searchedData);
    } catch (e) {
        res.status(400).send(e);
    };
};

exports.getById = async (req, res, next) => {
    try {
        var searchedData = await CustomerDAO.getById(req.params.id)
        res.status(200).send(searchedData);
    } catch (e) {
        res.status(400).send(e);
    };
};


exports.post = async (req, res, next) => {
    validate();
    try {
        const data = await CustomerDAO.create({
            name: req.body.name,
            email: req.body.email,
            password: md5(req.body.password + global.SALT_KEY)
        })
        emailService.send(req.body.email,
            'Bem vindo ao shop service',
            global.EMAIL_TMPL.replace('{0}', req.body.name));

        res.status(200).send(data);
    } catch (e) {
        res.status(400).send(e);
    }
};

exports.authenticate = async (req, res, next) => {
    validate();
    try {
        const customer = await repository.authenticate({
            email: req.body.email,
            password: md5(req.body.password + global.SALT_KEY)
        });

        if (!customer) {
            res.status(404).send({
                message: 'Usuário ou senha inválidos',
                messageErro: e
            });
            return;
        }

        const token = await authService.generateToken({
            email: customer.email,
            name: customer.name
        });

        res.status(200).send({
            token: token,
            data: {
                email: customer.email,
                name: customer.name
            }
        });
    } catch (e) {
        res.status(500).send({
            messageErro: e,
            message: 'Ocorreu um erro ao realizar a operação'
        });
    }
};

exports.put = async (req, res, next) => {
    validate();
    try {
        const data = await CustomerDAO.update(req.params.id, req.body)
        res.status(200).send({
            msg: 'Operação realizada com sucesso'
        });
    } catch (error) {
        res.status(500).send({
            msg: 'Ocorreu um erro na operação',
            messageErro: error
        });

    }
};

exports.delete = async (req, res, next) => {
    await CustomerDAO.delete(req.params.id)
        .then(() => {
            res.status(200).send({
                message: 'Operação realizada com sucesso'
            });
        }).catch(e => {
            res.status(500).send({
                message: 'Ocorreu um erro na operação',
                msg: e
            });
        });
};

function validate() {
    let validator = new ValidatorContract();
    validator.hasMin(req.body.slug, 3, 'O tamanho minimo para o campo é 3 caracterers');
    validator.hasMax(req.body.slug, 50, 'O tamanho maximo para o campo é 50 caracterers');

    if (!validator.isValid()) {
        res.status(400).send(validator.errors()).end();
        return;
    }
}