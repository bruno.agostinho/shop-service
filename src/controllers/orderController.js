'use strict'

const ValidatorContract = require('../validator/validator');
const OrderDAO = require('../dao/orderDAO');
const guid = require('guid');


exports.get = async (req, res, next) => {
    try {
        var searchedData = await OrderDAO.get();
        res.status(200).send(searchedData);
    } catch (e) {
        res.status(500).send({
            messageErro: e,
            message: 'Erro ao realizar operação'
        });
    }
}

exports.getByNumber = async (req, res, next) => {
    try {
        var searchedData = await OrderDAO.getByNumber(req.params.number);
        res.status(200).send(searchedData);
    } catch (e) {
        res.status(400).send(e);
    };
};

exports.getById = async (req, res, next) => {
    try {
        var searchedData = await OrderDAO.getById(req.params.id);
        res.status(200).send(searchedData);
    } catch (e) {
        res.status(400).send(e);
    };
};


exports.post = async (req, res, next) => {
    validate();
    try {
        const data = await OrderDAO.create({
            number: guid.raw().substring(0, 9),
            customer: req.body.customer,
            items: req.body.items
        });
        res.status(200).send(data);
    } catch (e) {
        res.status(400).send(e);
    }
};

exports.put = async (req, res, next) => {
    try {
        const data = await OrderDAO.update(req.params.id, req.body);
        res.status(200).send({
            data: data,
            msg: 'Operação realizada com sucesso'
        });
    } catch (error) {
        res.status(500).send({
            msg: 'Ocorreu um erro na operação',
            messageErro: error
        });

    }
};

exports.delete = async (req, res, next) => {
    await OrderDAO.delete(req.params.id)
        .then(() => {
            res.status(200).send({
                message: 'Operação realizada com sucesso'
            });
        }).catch(e => {
            res.status(500).send({
                message: 'Ocorreu um erro na operação',
                msg: e
            });
        });
};

function validate() {
    let validator = new ValidatorContract();
    validator.hasMin(req.body.slug, 1, 'O tamanho minimo para o campo é 3 caracterers');
    validator.hasMax(req.body.slug, 10, 'O tamanho maximo para o campo é 50 caracterers');

    if (!validator.isValid()) {
        res.status(400).send(validator.errors()).end();
        return;
    }
}