'use strict'

const ValidatorContract = require('../validator/validator');
const ProductDAO = require('../dao/productDAO');


exports.get = async (req, res, next) => {
    try {
        var searchedData = await ProductDAO.get();
        res.status(200).send(searchedData);
    } catch (e) {
        res.status(500).send({
            messageErro: e,
            message: 'Erro ao realizar operação'
        });
    }
}

exports.getBySlug = async (req, res, next) => {
    try {
        var searchedData = await ProductDAO.getBySlug(req.params.slug)
        res.status(200).send(searchedData);
    } catch (e) {
        res.status(400).send(e);
    };
};

exports.getOneBySlug = async (req, res, next) => {
    try {
        var searchedData = await ProductDAO.getOneBySlug(req.params.slug)
        res.status(200).send(searchedData);
    } catch (e) {
        res.status(400).send(e);
    };
};

exports.getById = async (req, res, next) => {
    try {
        var searchedData = await ProductDAO.getById(req.params.id)
        res.status(200).send(searchedData);
    } catch (e) {
        res.status(400).send(e);
    };
};

exports.getByTag = async (req, res, next) => {
    try {
        var searchedData = await ProductDAO.find(req.params.tag)
        res.status(200).send(searchedData);
    } catch (e) {
        res.status(400).send(e);
    };
};

exports.post = async (req, res, next) => {
    validate();
    try {
        const data = await ProductDAO.create(req.body)
        res.status(200).send(data);
    } catch (e) {
        res.status(400).send(e);
    }
};

exports.put = async (req, res, next) => {
    validate();
    try {
        const data = await ProductDAO.update(req.params.id, req.body)
        res.status(200).send({
            itemAlterado: x,
            data: data,
            msg: 'Operação realizada com sucesso'
        });
    } catch (error) {
        res.status(500).send({
            msg: 'Ocorreu um erro na operação',
            messageErro: error
        });

    }
};

exports.delete = async (req, res, next) => {
    await ProductDAO.delete(req.params.id)
        .then(() => {
            res.status(200).send({
                message: 'Operação realizada com sucesso'
            });
        }).catch(e => {
            res.status(500).send({
                message: 'Ocorreu um erro na operação',
                msg: e
            });
        });
};

function validate() {
    let validator = new ValidatorContract();
    validator.hasMin(req.body.slug, 3, 'O tamanho minimo para o campo é 3 caracterers');
    validator.hasMax(req.body.slug, 50, 'O tamanho maximo para o campo é 50 caracterers');

    if (!validator.isValid()) {
        res.status(400).send(validator.errors()).end();
        return;
    }
}